#include "validator.h"
#include "io.h"
#include "rotation.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv) {
    int result_status;
    char *source_image;
    char *transformed_image;
    enum rotation_angle angle;
    struct image img;

    //validate args
    result_status = validate_args(argc, argv, &source_image, &transformed_image, &angle);
    if (result_status != VALIDATION_IS_OK) {
        fprintf(stderr, "validating arguments failed with code: %i\n", result_status);
        return result_status;
    }

    //read img
    result_status = read_bmp_file(source_image, &img);
    if (result_status != READ_OK) {
        fprintf(stderr, "reading bmp file failed with code: %i\n", result_status);
        free(img.data);
        return result_status;
    }

    //rotate img
    img = rotate_by_angle(img, angle);

    //write img
    result_status = write_bmp_file(transformed_image, &img);
    if (result_status != WRITE_OK) {
        fprintf(stderr, "writing to bmp file failed with code: %i\n", result_status);
        free(img.data);
        return result_status;
    }

    //free resources
    free(img.data);

    return 0;
}
