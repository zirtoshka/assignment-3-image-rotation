#include "validator.h"
#include "util.h"

enum validate_args_status validate_args(int argc, char **argv, char** source_image, char** transformed_image, enum rotation_angle* angle){
    if(validate_arg_count(argc)!=COUNT_OF_ARGS_IS_OK){
        return VALIDATION_IS_NOT_OK_BECAUSE_OF_ARGS_COUNT;
    }

    *source_image=argv[1];
    *transformed_image=argv[2];

    if(validate_angle_from_args(argv[3], angle)!=ANGLE_IS_OK){
        return VALIDATION_IS_NOT_OK_BECAUSE_OF_ANGLE;
    }

    return VALIDATION_IS_OK;

}

enum validate_angle_status validate_angle_from_args(char* angle_from_args, enum rotation_angle* angle){
    if (!is_digit_str(angle_from_args)){
        return ANGLE_IS_NOT_INT;
    }
    int parsed_angle = parsing_angle(angle_from_args);
    if (parsed_angle%90!=0){
        return ANGLE_IS_NOT_DIV_90;
    }
    *angle=parsed_angle/90;
    return ANGLE_IS_OK;
}

enum validate_arg_count_status validate_arg_count(int argc){
    if (argc<4){
        return COUNT_OF_ARGS_IS_NOT_ENAUGH;
    }
    if (argc>4){
        return COUNT_OF_ARGS_IS_TO_MANY;
    }
    return COUNT_OF_ARGS_IS_OK;
}
