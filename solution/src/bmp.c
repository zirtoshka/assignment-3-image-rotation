#include "bmp.h"
#include "util.h"
#include <stdlib.h>

#define BMP_FILE_CODE 0x4D42

#define HEADER_SIZE 40
#define HEADER_PLANES 1
#define HEADER_BIT_COUNT 24
#define HEADER_COMPRESSION 0
#define HEADER_X_PELS_PER_METERS 2834
#define HEADER_Y_PELS_PER_METERS 2834
#define HEADER_CLR_USED 0
#define HEADER_CLR_IMPORTANT 0


enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;
    char *bgr;
    if (fread(&header, sizeof(struct bmp_header), 1, in) == 1) {
        if (header.bfType != BMP_FILE_CODE || header.biBitCount != HEADER_BIT_COUNT)
            return READ_WRONG_HEADER;
        bgr = malloc(header.biSizeImage);
        if (bgr == NULL) return READ_NOT_MALLOC;
        if (fread(bgr, header.biSizeImage, 1, in) == 1) {
            *img = new_image(header.biWidth, header.biHeight, bgr);
            free(bgr);
            return READ_OK;
        } else {
            free(bgr);
            return READ_WRONG_DATA;
        }
    } else {
        return READ_WRONG_HEADER;
    }
}

static struct bmp_header new_header(struct image const *img) {
    uint32_t biSizeImage = rounding_by_four(img->width * SIZE_OF_PIXEL) * img->height;
    return (struct bmp_header) {.bfType = BMP_FILE_CODE,
            .bfileSize = biSizeImage + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = HEADER_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = HEADER_PLANES,
            .biBitCount = HEADER_BIT_COUNT,
            .biCompression = HEADER_COMPRESSION,
            .biSizeImage = biSizeImage,
            .biXPelsPerMeter = HEADER_X_PELS_PER_METERS,
            .biYPelsPerMeter = HEADER_Y_PELS_PER_METERS,
            .biClrUsed = HEADER_CLR_USED,
            .biClrImportant = HEADER_CLR_IMPORTANT};
}


enum write_status to_bmp(FILE *out, struct image const *img) {
    char *bgr;
    struct bmp_header header = new_header(img);
    const uint_fast32_t img_size = rounding_by_four(img->width * SIZE_OF_PIXEL) * img->height;
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) == 1) {
        bgr = to_char(*img);
        if (fwrite(bgr, 1, img_size, out) == img_size) {
            free(bgr);
            return WRITE_OK;
        } else {
            free(bgr);
            return WRITE_WRONG_DATA;
        }
    } else {
        return WRITE_WRONG_HEADER;
    }
}
