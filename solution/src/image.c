#include "image.h"
#include "util.h"
#include <stdlib.h>

static void pixel_to_bgr(struct pixel pix, char* bgr);


static uint32_t remaining_pixels(const uint32_t width) {
    return rounding_by_four(width * SIZE_OF_PIXEL) - width * SIZE_OF_PIXEL;
}


static struct pixel new_pixel(char *const bgr) {
    return (struct pixel) {bgr[0], bgr[1], bgr[2]};
}

struct image new_image(const uint32_t width, const uint32_t height, char *bgr) {
    uint32_t r_pixels = remaining_pixels(width);
    struct pixel *result = malloc(width * height * SIZE_OF_PIXEL);
    if (result == NULL) return (struct image) {0};
    struct image img = (struct image) {width, height, result};

    for (uint32_t i = 0; i < height; i++) {
        for (uint_fast32_t j = 0; j < width; j++) {
            result[GET_DATA_INDEX(width, i, j)] = new_pixel(bgr);
            bgr += SIZE_OF_PIXEL;
        }
        bgr += r_pixels;
    }

    return img;
}

char *to_char(struct image img) {
    uint32_t r_pixels = remaining_pixels(img.width);
    char *const chars = malloc(rounding_by_four(img.width * SIZE_OF_PIXEL) * img.height);
    if (chars == NULL) return NULL;
    const uint32_t row_length = rounding_by_four(img.width * SIZE_OF_PIXEL);
    const uint32_t width = img.width;
    const struct pixel *data = img.data;

    for (uint32_t i = 0; i < img.height; i++) {
        for (uint32_t j = 0; j < width; j++) {
            pixel_to_bgr(data[i * width + j], &chars[i * row_length + j * SIZE_OF_PIXEL]);
        }
        for (uint32_t j = 0; j < r_pixels; j++)
            chars[i * row_length + width * SIZE_OF_PIXEL + j] = 0;
    }

    return chars;
}


static void pixel_to_bgr(const struct pixel pix, char* bgr) {
    *(bgr) = (char) pix.b;
    *(bgr + 1) = (char) pix.g;
    *(bgr + 2) = (char) pix.r;
}
