#include "io.h"
#include "bmp.h"
#include <stdio.h>

enum read_status read_bmp_file(char *source, struct image *img) {
    FILE *file;
    if (source) {
        file = fopen(source, "rb");
        if (file) {
            enum read_status status = from_bmp(file, img);
            fclose(file);
            return status;
        } else {
            return READ_WRONG_OPENING;
        }

    } else {
        return READ_WRONG_NAME_OF_FILE;
    }
}

enum write_status write_bmp_file(char *result, struct image *img){
    FILE *file;
    if (result) {
        file = fopen(result, "wb");
        if (file) {
            enum write_status status = to_bmp(file, img);
            if (fclose(file))
                return WRITE_WRONG_DATA;
            return status;
        } else {
            return WRITE_WRONG_OPENING;
        }

    } else {
        return WRITE_WRONG_NAME_OF_FILE;
    }
}
