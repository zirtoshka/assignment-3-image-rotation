#include "rotation.h"
#include <stdlib.h>

static struct image rotate_by_angle_90(struct image const source) {
    const uint32_t width = source.width;
    const uint32_t height = source.height;
    struct image new_image = {.width=height, .height=width, .data=malloc(source.width * source.height * SIZE_OF_PIXEL)};
    if (new_image.data == NULL) return source;

    for (uint32_t row = 0; row < height; row++)
        for (uint32_t column = 0; column < width; column++)
            new_image.data[GET_DATA_INDEX(height, width - column - 1, row)] = source.data[GET_DATA_INDEX(width, row,
                                                                                                         column)];

    free(source.data);
    return new_image;
}

static struct image rotate_by_angle_180(struct image const source) {
    const uint32_t width = source.width;
    const uint32_t height = source.height;
    struct image new_image = {.width=width, .height=height, .data=malloc(source.width * source.height * SIZE_OF_PIXEL)};
    if (new_image.data == NULL) return source;

    for (uint32_t row = 0; row < height; row++)
        for (uint32_t column = 0; column < width; column++)
            new_image.data[GET_DATA_INDEX(width, height-row-1, width-column-1)] = source.data[GET_DATA_INDEX(width, row, column)];

    free(source.data);
    return new_image;
}

static struct image rotate_by_angle_270(struct image const source) {
    const uint32_t width = source.width;
    const uint32_t height = source.height;
    struct image new_image = {.width=height, .height=width, .data=malloc(source.width * source.height * SIZE_OF_PIXEL)};
    if (new_image.data == NULL) return source;

    for (uint32_t row = 0; row < height; row++)
        for (uint32_t column = 0; column < width; column++)
            new_image.data[GET_DATA_INDEX(height, column, height-row-1)] = source.data[GET_DATA_INDEX(width, row,
                                                                                                      column)];

    free(source.data);
    return new_image;
}

struct image rotate_by_angle(struct image source, enum rotation_angle angle) {
    struct image result;

    switch (angle) {
        case NINETY_DEGREES:
            result = rotate_by_angle_90(source);
            break;

        case ONE_HUNDRED_EIGHTY_DEGREES:
            result = rotate_by_angle_180(source);
            break;

        case TWO_HUNDRED_SEVENTY_DEGREES:
            result = rotate_by_angle_270(source);
            break;

        default:
            return source;
    }

    return result;
}
