
#include "util.h"
#include <ctype.h>
#include <stdlib.h>
#include <string.h>


int parsing_angle(char *angle) {
    int result = atoi(angle);
    while (result < 0) {
        result += 360;
    }
    if (result >= 360) {
        result %= 360;
    }
    return result;
}

bool is_digit_str(char *str) {
    if (!str)
        return false;

    if (!isdigit(str[0]) && str[0] != '-') {
        return false;
    }

    for (size_t i = 1; i < strlen(str); i++) {
        if (!isdigit(str[i])) {
            return false;
        }

    }
    return true;

}

uint32_t rounding_by_four(uint32_t n) {
    if (n % 4 != 0)
        return (n / 4 + 1) * 4;
    return n;
}

