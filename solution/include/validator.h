#include "rotation.h"
#include "status.h"

#ifndef VALIDATOR_H
#define VALIDATOR_H

enum validate_args_status validate_args(int argc, char **argv, char** source_image, char** transformed_image, enum rotation_angle* angle);

enum validate_angle_status validate_angle_from_args(char* angle_from_args, enum rotation_angle* angle);

enum validate_arg_count_status validate_arg_count(int argc);


#endif
