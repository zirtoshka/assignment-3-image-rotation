#ifndef STATUS_H
#define STATUS_H

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_WRONG_NAME_OF_FILE,
    READ_WRONG_OPENING,
    READ_WRONG_HEADER,
    READ_WRONG_DATA,
    READ_NOT_MALLOC
    /* коды других ошибок  */
};

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_WRONG_NAME_OF_FILE,
    WRITE_WRONG_OPENING,
    WRITE_WRONG_HEADER,
    WRITE_WRONG_DATA
    /* коды других ошибок  */
};

enum validate_args_status {
    VALIDATION_IS_OK = 0,
    VALIDATION_IS_NOT_OK_BECAUSE_OF_ANGLE,
    VALIDATION_IS_NOT_OK_BECAUSE_OF_ARGS_COUNT
};

enum validate_angle_status {
    ANGLE_IS_OK = 0,
    ANGLE_IS_NOT_INT,
    ANGLE_IS_NOT_DIV_90
};

enum validate_arg_count_status {
    COUNT_OF_ARGS_IS_OK = 0,
    COUNT_OF_ARGS_IS_NOT_ENAUGH,
    COUNT_OF_ARGS_IS_TO_MANY
};




#endif
