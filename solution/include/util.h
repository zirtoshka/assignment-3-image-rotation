#include <stdbool.h>
#include <stdint.h>

#ifndef UTIL_H
#define UTIL_H



int parsing_angle(char* angle);

bool is_digit_str(char* str);

uint32_t rounding_by_four(uint32_t n);

#endif

