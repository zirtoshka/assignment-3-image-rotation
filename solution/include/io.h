#include "image.h"
#include "status.h"

#ifndef IO_H
#define IO_H

enum read_status read_bmp_file(char *source, struct image *img);

enum write_status write_bmp_file(char *result, struct image *img);

#endif
