#include "image.h"

#ifndef ROTATION_H
#define ROTATION_H

enum rotation_angle {
    ZERO_DEGREES = 0,
    NINETY_DEGREES,
    ONE_HUNDRED_EIGHTY_DEGREES,
    TWO_HUNDRED_SEVENTY_DEGREES
};

struct image rotate( struct image const source );

struct image rotate_by_angle( struct image const source, enum rotation_angle angle);




#endif
