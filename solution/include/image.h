#include <stdint.h>

#ifndef IMAGE_H
#define IMAGE_H

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

#define SIZE_OF_PIXEL (sizeof(struct pixel))
#define GET_DATA_INDEX(width, row, column) ((row) * (width) + (column))

struct image {
    uint32_t width, height;
    struct pixel *data;
};

struct image new_image(uint32_t width, uint32_t height, char* bgr);

char *to_char(struct image img);


#endif
